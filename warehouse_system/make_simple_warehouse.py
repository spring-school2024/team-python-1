import os

from cells import *
from warehouse_system.warehouse import Warehouse
from robot.robot import Robot

warehouse = Warehouse("simple_warehouse", 3, 3)

warehouse.setCell(CornerCell(True, False, False, True), 0, 0)
warehouse.setCell(CornerCell(False, False, True, True), 0, 1)
warehouse.setCell(EmptyCell(True, True, True, True), 0, 2)

warehouse.setCell(HallwayCell(True, False, True, False), 1, 0)
warehouse.setCell(TCrossingCell(True, False, False, False), 1, 1)
warehouse.setCell(RampCell(False, True, True, True), 1, 2)

warehouse.setCell(CornerCell(True, True, False, False), 2, 0)
warehouse.setCell(CornerCell(False, True, True, False), 2, 1)
warehouse.setCell(EmptyCell(True, True, True, True), 2, 2)

robot_names = ["willie", "william", "travis", "tim", "thomas", "tessa", "susan", "sandra"]
for name in robot_names:
    robot = Robot(name, warehouse)

    if not os.path.isdir(f'maps/{name}'):
        os.makedirs(f'maps/{name}')

    robot.save(robot.warehouse.name)
