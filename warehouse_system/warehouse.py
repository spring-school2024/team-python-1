from typing import Optional

import numpy as np
import math
import pickle
from cells import *
import ast
from string import ascii_lowercase


class Warehouse:
    def __init__(self, name: str, width: int, height: int):
        self.name = name
        self.width = width
        self.height = height
        self.cells = np.empty(shape=(self.height, self.width), dtype=Cell)

    def setCell(self, cell: Cell, x, y):
        self.cells[y][x] = cell

    def getCell(self, x, y) -> Cell:
        return self.cells[y][x]

    def getCellByIdentifier(self, coordinate) -> Cell:
        x = ord(coordinate[0:1]) - 97
        y = ast.literal_eval(coordinate[1:2]) 
        return self.cells[x][y]
    
    def getMovementPathKiss(self, x, y, coordinate):
        actualX = x
        actualY = y
        actualCell = self.getCell(actualX,actualY)
        
        targetCellX = ord(coordinate[0:1]) - 97
        targetCellY = ast.literal_eval(coordinate[1:2]) 
        targetCell = self.getCellByIdentifier(coordinate)

        if actualCell == targetCell:
            return []
        
        directions = ["north", "east", "south", "west"]
        visitedCells = [actualCell]
        solution = []     
        while True:
            for dir in directions:
                if dir == "north" and not actualCell.has_wall_north and not visitedCells.index(self.getCell(actualX, actualY - 1)):
                    solution.append(dir)
                    actualY = actualY - 1 
                    actualCell = self.getCell(actualX, actualY)
                    break
                elif dir == "east" and not actualCell.has_wall_east:
                    solution.append(dir)
                    actualX = actualX + 1 
                    actualCell = self.getCell(actualX, actualY)
                    break
                elif dir == "south" and not actualCell.has_wall_south:
                    solution.append(dir)
                    actualY = actualY + 1 
                    actualCell = self.getCell(actualX, actualY)
                    break
                elif dir == "west" and not actualCell.has_wall_west:
                    solution.append(dir)
                    actualX = actualX - 1 
                    actualCell = self.getCell(actualX, actualY)
                    break

            visitedCells.append(actualCell)
            if actualCell == targetCell:
                break

        return solution
    
    def get_all_paths(self, cell, target, x, y, path=None, steps=None, lastStep=None):

        directions = ["north", "east", "south", "west"]
        paths = []
        if path is None:
            path = []
        path.append(cell)

        stepSequences = []
        if steps is None:
            steps = []
        if lastStep is not None:
            steps.append(lastStep)
        
        if cell == target:
            paths.append(path)
            stepSequences.append(steps)
            return stepSequences

        validDirections = []
        for dir in directions:
            if dir == "north" and not self.getCell(x, y).has_wall_north and not self.getCell(x, y - 1) in path:
                validDirections.append("north")
            elif dir == "east" and not self.getCell(x, y).has_wall_east and not self.getCell(x + 1, y) in path:
                validDirections.append("east")
            elif dir == "south" and not self.getCell(x, y).has_wall_south and not self.getCell(x, y + 1) in path:
                validDirections.append("south")
            elif dir == "west" and not self.getCell(x, y).has_wall_west and not self.getCell(x - 1, y) in path:
                validDirections.append("west")

        
        if len(validDirections):
            for validDir in validDirections:
                if validDir == "north":
                    stepSequences.extend(self.get_all_paths(self.getCell(x, y - 1), target, x, y - 1, path[:], steps[:], "north"))
                elif validDir == "east":
                    stepSequences.extend(self.get_all_paths(self.getCell(x + 1, y), target, x + 1, y, path[:], steps[:], "east"))
                elif validDir == "south":
                    stepSequences.extend(self.get_all_paths(self.getCell(x, y + 1), target, x, y + 1, path[:], steps[:], "south"))
                elif validDir == "west":
                    stepSequences.extend(self.get_all_paths(self.getCell(x - 1, y), target, x - 1, y, path[:], steps[:], "west"))           
                
        return stepSequences
    
    def get_shortest_seq(self, cell, current_position, targetCoordinate):
        
        targetCellX = ord(targetCoordinate[0:1]) - 97
        targetCellY = ast.literal_eval(targetCoordinate[1:2]) 
        targetCell = self.getCellByIdentifier(targetCoordinate)

        steps = self.get_all_paths(cell, targetCell, current_position[1], current_position[0], None)
        shortestStepSequence = None
        shortestStepSequenceLength = 100000
        for seq in steps:
            if len(seq) < shortestStepSequenceLength:
                shortestStepSequence = seq
                shortestStepSequenceLength = len(seq)

        return shortestStepSequence
 
    def getTotalCapacity(self):
        totalCapacity = 0
        for i in range(0, self.width):
            for j in range(0, self.height):
                loc = self.getCell(i, j)
                if loc is not None:
                    totalCapacity += loc.capacity

        return totalCapacity

    def save(self, filename):
        with open(f'{filename}.pickle', 'wb') as handle:
            pickle.dump(self, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load(filename):
        with open(filename, 'rb') as handle:
            warehouse = pickle.load(handle)

        return warehouse

    def warehouseState(self, robot_location) -> str:
        # Print the column headers
        
        output = []

        output.append("   ")
        for c in range(self.width):
            output.append(f"{c} ")
        output.append("\n")

        for y in range(self.height * 2):
            if y % 2 == 0:  # horizontal lines
                output.append("  +")
                for x in range(self.width):
                    cell = self.getCell(x, y // 2)
                    if cell.has_wall_north:
                        output.append("-+")
                    else:
                        output.append(" +")
            else:  # vertical lines and cells
                output.append(chr(65 + y // 2) + " ")
                for x in range(self.width):
                    cell = self.getCell(x, y // 2)
                    
                    if not isinstance(cell, EmptyCell):
                        
                        if cell.has_wall_west:
                            output.append("|")
                        else:
                            output.append(" ")

                        if robot_location[0] == y // 2 and robot_location[1] == x:
                            output.append("o")
                        else:
                            output.append(" ")

                        if x == self.width - 1 and cell.has_wall_east:
                            output.append("|")
                    else:
                        prevCell = self.getCell(x-1, y // 2)
                        cell =self.getCell(x, y // 2)
                        if prevCell.has_wall_east and x != 0:
                            output.append("| ")
                        else:
                            output.append("  ")
            output.append("\n")

        output.append(" ")
        for l in range(0, self.width):
            cell = self.getCell(l, self.height - 1)
        
            if not isinstance(cell, EmptyCell):
                if cell.has_wall_west:
                    output.append(" +")
                if cell.has_wall_south:
                    output.append("-+")
                else:
                    output.append(" +")
            else:
                output.append("  ")
        output.append("\n")

        return ''.join(output)


