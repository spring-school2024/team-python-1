class HelloWorld():
    def __init__(self):
        pass

    def hello(self):
        return "world"
    
    def greet(self, name):
        return f"hello {name}"