from commands.command import Command

from typing import Dict


class CLI:
    def __init__(self, commands: Dict):
        self.commands = commands

    def get_command(self, command_line_arguments) -> Command:
        if len(command_line_arguments) != 2:
            raise ValueError("Only one argument is allowed.")

        return self.commands[command_line_arguments[1].lower()]
