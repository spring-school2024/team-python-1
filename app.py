import sys
from warehouse_system.hello_world import HelloWorld
from cli import CLI
from commands.command_map import CommandMap
from robot.robot import Robot
from warehouse_system.warehouse import Warehouse

WAREHOUSE_API_URL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/"


def print_hello_world():
    print("Hello world.")


if __name__ == '__main__':
    session_id = "willie"

    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")

    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    command = cli.get_command(["", "state"])
    result = command.execute()

    command = cli.get_command(sys.argv)
    result = command.execute()

    shelf_id, walls, shelf_inventory = robot.scan_near()
