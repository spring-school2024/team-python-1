import pytest

from cells.hallway_cell import HallwayCell


def test_hallway_cell():
    cell = HallwayCell(True, False, True, False)
    assert isinstance(cell, HallwayCell)

    assert cell.name == "Hallway"
    assert cell.capacity == 9

    assert cell.has_wall_east is False
    assert cell.has_wall_west is False
    assert cell.has_wall_south is True
    assert cell.has_wall_north is True


def test_hallway_cell_invalid_number_of_walls():
    with pytest.raises(AttributeError):
        cell = HallwayCell(True, True, True, False)


def test_hallway_cell_invalid_wall_positions():
    with pytest.raises(AttributeError):
        cell = HallwayCell(True, True, False, False)
