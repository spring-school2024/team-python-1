import pytest

from cells.t_crossing_cell import TCrossingCell


def test_t_crossing_cell():
    cell = TCrossingCell(False, True, False, False)
    assert isinstance(cell, TCrossingCell)

    assert cell.name == "T-Crossing"
    assert cell.capacity == 6

    assert cell.has_wall_east is True
    assert cell.has_wall_west is False
    assert cell.has_wall_south is False
    assert cell.has_wall_north is False


def test_t_crossing_cell_invalid():
    with pytest.raises(AttributeError):
        cell = TCrossingCell(False, True, True, False)
