import pytest

from cells.x_crossing_cell import XCrossingCell


def test_x_crossing_cell():
    cell = XCrossingCell(False, False, False, False)
    assert isinstance(cell, XCrossingCell)

    assert cell.name == "X-Crossing"
    assert cell.capacity == 4

    assert cell.has_wall_east is False
    assert cell.has_wall_west is False
    assert cell.has_wall_south is False
    assert cell.has_wall_north is False


def test_x_crossing_cell_invalid():
    with pytest.raises(AttributeError):
        cell = XCrossingCell(False, True, False, False)
