import pytest

from cells.corner_cell import CornerCell


def test_corner_cell():
    cell = CornerCell(False, False, True, True)
    assert isinstance(cell, CornerCell)

    assert cell.name == "Corner"
    assert cell.capacity == 8

    assert cell.has_wall_east is False
    assert cell.has_wall_west is True
    assert cell.has_wall_south is True
    assert cell.has_wall_north is False


def test_corner_cell_invalid_number_of_walls():
    with pytest.raises(AttributeError):
        cell = CornerCell(True, True, True, False)


def test_corner_cell_invalid_wall_positions():
    with pytest.raises(AttributeError):
        cell = CornerCell(True, False, True, False)
