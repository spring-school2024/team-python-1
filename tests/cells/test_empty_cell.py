import pytest

from cells.empty_cell import EmptyCell


def test_empty_cell():
    cell = EmptyCell(True, True, True, True)
    assert isinstance(cell, EmptyCell)

    assert cell.name == "Empty"
    assert cell.capacity == 0

    assert cell.has_wall_east is True
    assert cell.has_wall_west is True
    assert cell.has_wall_south is True
    assert cell.has_wall_north is True


def test_empty_invalid():
    with pytest.raises(AttributeError):
        cell = EmptyCell(False, True, False, False)
