import pytest

from cells.dead_end_cell import DeadEndCell


def test_dead_end_cell():
    cell = DeadEndCell(False, True, True, True)
    assert isinstance(cell, DeadEndCell)

    assert cell.name == "Dead-End"
    assert cell.capacity == 12

    assert cell.has_wall_east is True
    assert cell.has_wall_west is True
    assert cell.has_wall_south is True
    assert cell.has_wall_north is False


def test_dead_end_cell_invalid_number_of_walls():
    with pytest.raises(AttributeError):
        cell = DeadEndCell(True, True, False, False)