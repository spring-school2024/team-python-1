import pytest

from cells.ramp_cell import RampCell


def test_ramp_cell():
    cell = RampCell(True, True, True, False)
    assert isinstance(cell, RampCell)

    assert cell.name == "Ramp"
    assert cell.capacity == 0

    assert cell.has_wall_east is True
    assert cell.has_wall_west is False
    assert cell.has_wall_south is True
    assert cell.has_wall_north is True


def test_ramp_cell_invalid():
    with pytest.raises(AttributeError):
        cell = RampCell(False, False, False, False)
