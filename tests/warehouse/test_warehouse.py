import numpy as np
from warehouse_system.warehouse import Warehouse
from cells import *


def test_warehouse_set_location():
    cell1 = CornerCell(True, True, False, False)
    warehouse = Warehouse("test", 10, 10)
    warehouse.setCell(cell1, 1, 1)
    cell2 = warehouse.cells[1][1]
    assert cell1 == cell2


def test_warehouse_get_location():
    cell1 = CornerCell(True, True, False, False)
    warehouse = Warehouse("test", 10, 10)
    warehouse.setCell(cell1, 1, 1)
    cell2 = warehouse.getCell(1, 1)
    assert cell1 == cell2


def test_warehouse_capacity():
    cell1 = CornerCell(True, True, False, False)
    warehouse = Warehouse("test", 10, 10)
    warehouse.setCell(cell1, 1, 1)
    capacity = warehouse.getTotalCapacity()
    assert capacity == 8


def test_load_save():
    robotLocation = np.array([3, 0])
    warehouse = Warehouse("test", 10, 10)
    warehouse.setCell(CornerCell(True, True, False, False), 3, 4)

    warehouse.save("filename")
    warehouseReload: Warehouse = warehouse.load("filename.pickle")

    assert warehouse.width == warehouseReload.width
    assert warehouse.height == warehouseReload.height

    assert isinstance(warehouseReload.getCell(3, 4), CornerCell)

def test_coordinate():
    robotLocation = np.array([3, 0])
    warehouse = Warehouse("test", 10, 10)
    warehouse.setCell(CornerCell(True, True, False, False), 2, 0)
    warehouse.setCell(TCrossingCell(True, False, False, False), 1, 1)

    assert warehouse.getCellByIdentifier("a1") == None
    assert isinstance(warehouse.getCellByIdentifier("a2"), CornerCell)
    assert warehouse.getCellByIdentifier("a2").name == "Corner"
    assert warehouse.getCellByIdentifier("a2").has_wall_east == True
    assert warehouse.getCellByIdentifier("a3") == None
    assert warehouse.getCellByIdentifier("b1").name == "T-Crossing"
    assert warehouse.getCellByIdentifier("b1").has_wall_east == False
    assert warehouse.getCellByIdentifier("b2") == None
    assert warehouse.getCellByIdentifier("b3") == None

def test_pathfinding(): 
    robot_location = np.array([0, 0])
    warehouse = Warehouse("test", 10, 10)

    warehouse.setCell(CornerCell(True, False, False, True), 0, 0)
    warehouse.setCell(CornerCell(False, False, True, True), 0, 1)
    warehouse.setCell(EmptyCell(True, True, True, True), 0, 2)

    warehouse.setCell(HallwayCell(True, False, True, False), 1, 0)
    warehouse.setCell(TCrossingCell(True, False, False, False), 1, 1)
    warehouse.setCell(RampCell(False, True, True, True), 1, 2)

    warehouse.setCell(CornerCell(True, True, False, False), 2, 0)
    warehouse.setCell(CornerCell(False, True, True, False), 2, 1)
    warehouse.setCell(EmptyCell(True, True, True, True), 2, 2)

    solution = warehouse.getMovementPathKiss(0, 0, "b1")

    assert len(solution) == 4
    assert solution[0] == "east"
    assert solution[1] == "east"
    assert solution[2] == "south"
    assert solution[3] == "west"

def test_getallpath(): 
    robot_location = np.array([0, 0])
    warehouse = Warehouse("test", 10, 10)

    warehouse.setCell(CornerCell(True, False, False, True), 0, 0)
    warehouse.setCell(CornerCell(False, False, True, True), 0, 1)
    warehouse.setCell(EmptyCell(True, True, True, True), 0, 2)

    warehouse.setCell(HallwayCell(True, False, True, False), 1, 0)
    warehouse.setCell(TCrossingCell(True, False, False, False), 1, 1)
    warehouse.setCell(RampCell(False, True, True, True), 1, 2)

    warehouse.setCell(CornerCell(True, True, False, False), 2, 0)
    warehouse.setCell(CornerCell(False, True, True, False), 2, 1)
    warehouse.setCell(EmptyCell(True, True, True, True), 2, 2)

    steps = warehouse.get_all_paths(warehouse.getCell(0, 0), warehouse.getCell(1, 1), 0, 0, None)

    assert len(steps) == 2

def test_geshortestseq(): 
    robot_location = np.array([0, 0])
    warehouse = Warehouse("test", 10, 10)

    warehouse.setCell(CornerCell(True, False, False, True), 0, 0)
    warehouse.setCell(CornerCell(False, False, True, True), 0, 1)
    warehouse.setCell(EmptyCell(True, True, True, True), 0, 2)

    warehouse.setCell(HallwayCell(True, False, True, False), 1, 0)
    warehouse.setCell(TCrossingCell(True, False, False, False), 1, 1)
    warehouse.setCell(RampCell(False, True, True, True), 1, 2)

    warehouse.setCell(CornerCell(True, True, False, False), 2, 0)
    warehouse.setCell(CornerCell(False, True, True, False), 2, 1)
    warehouse.setCell(EmptyCell(True, True, True, True), 2, 2)

    steps = warehouse.get_shortest_seq(warehouse.getCell(0, 0), robot_location, "b1")

    assert len(steps) == 2





