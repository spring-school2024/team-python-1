import pytest
import numpy as np

from cells import *
from robot.robot import Robot
from api.warehouse_api import Warehouse_API
from api.james_api import James_API
from warehouse_system.warehouse import Warehouse


def test_api_call_successfull():
    answer = Warehouse_API().move("east")
    James_API().reset()

    assert answer.request_status == 'ok' or answer.request_status == 'failed'


@pytest.mark.parametrize("direction", ["north", "east", "west", "south"])
def test_robot_move_all_directions(direction):
    answer = Warehouse_API().move(direction)
    James_API().reset()

    assert answer.request_status == 'ok' or answer.request_status == 'failed'


def test_api_call_robot_not_known():
    answer = Warehouse_API("hansi").get_status()

    assert answer.request_status == 'failed'


@pytest.mark.parametrize("robotname", ["willie", "william", "travis", "tim", "thomas", "tessa", "susan", "sandra"])
def test_api_call_all_robots_known(robotname):
    answer = Warehouse_API(robotname).get_status()

    assert answer.request_status == 'ok' and answer.id == robotname


def test_robot_move_against_wall_api():
    James_API().reset()
    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    assert answer.walls["east"] == False
    assert answer.walls["south"] == False
    assert answer.walls["west"] == True
    assert answer.walls["north"] == True

    answerMove = Warehouse_API().move("west")
    assert answerMove.request_status == "failed" and answerMove.message == "poking_against_wall"

    answerMove = Warehouse_API().move("north")
    assert answerMove.request_status == "failed" and answerMove.message == "poking_against_wall"


def test_robot_move_against_wall_robot():
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    assert answer.walls["east"] == False
    assert answer.walls["south"] == False
    assert answer.walls["west"] == True
    assert answer.walls["north"] == True

    answerMove = robot.move("west")
    assert not answerMove.startswith("Successfully")

    answerMove = robot.move("north")
    assert not answerMove.startswith("Successfully")


def test_robot_move_successful_robot():
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    assert answer.walls["east"] == False
    assert answer.walls["south"] == False
    assert answer.walls["west"] == True
    assert answer.walls["north"] == True

    answerMove = robot.move("south")
    assert answerMove.startswith("Successfully")

    answerMove = robot.move("east")
    assert answerMove.startswith("Successfully")


def test_robot_move_east_after_scan():
    James_API().reset()
    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    assert answer.walls["east"] == False
    assert answer.walls["south"] == False
    assert answer.walls["west"] == True
    assert answer.walls["north"] == True

    answerMove = Warehouse_API().move("east")
    assert answerMove.request_status == "ok"


def test_get_capacity():
    James_API().reset()
    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    assert answer.walls["east"] == False
    assert answer.walls["south"] == False
    assert answer.walls["west"] == True
    assert answer.walls["north"] == True

    capacitySum = answer.max_capacity

    # (0,1)
    answerMove = Warehouse_API().move("east")
    assert answerMove.request_status == "ok"

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    capacitySum = capacitySum + answer.max_capacity

    assert capacitySum == 17

    # (0,2)
    answerMove = Warehouse_API().move("east")
    assert answerMove.request_status == "ok"

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    capacitySum = capacitySum + answer.max_capacity

    assert capacitySum == 25

    # (1,2)
    answerMove = Warehouse_API().move("south")
    assert answerMove.request_status == "ok"

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    capacitySum = capacitySum + answer.max_capacity

    assert capacitySum == 33

    # (1,1)
    answerMove = Warehouse_API().move("west")
    assert answerMove.request_status == "ok"

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    capacitySum = capacitySum + answer.max_capacity

    assert capacitySum == 39

    # (1,0)
    answerMove = Warehouse_API().move("west")
    assert answerMove.request_status == "ok"

    answer = Warehouse_API().scan_near()

    assert answer.request_status == 'ok'

    capacitySum = capacitySum + answer.max_capacity

    assert capacitySum == 47


def test_robot_wall_distance_at_startingpoint():
    James_API().reset()
    answer = Warehouse_API().scan_far()

    assert answer.request_status == 'ok'

    assert answer.scan_info["east"] == 2
    assert answer.scan_info["south"] == 1
    assert answer.scan_info["west"] == 0
    assert answer.scan_info["north"] == 0


def test_robot_scanfar_successful():
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    answer = robot.scan_far()

    assert answer.startswith("Successfully")


def test_robot_scanfar_failed():
    warehouse = Warehouse("test", 10, 10)
    answer = Robot("hansi", warehouse).scan_far()

    assert not answer.startswith("Successfully")


def test_robot_scan_near_successful():
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    shelf_id, walls, shelf_inventory = robot.scan_near()

    assert shelf_id == "Field"
    assert isinstance(walls, dict)
    assert isinstance(shelf_inventory, list)


def test_robot_scannear_failed():
    warehouse = Warehouse("test", 10, 10)
    answer = Robot("hansi", warehouse).scan_near()

    assert not answer.startswith("Successfully")


@pytest.mark.parametrize("input_data, expected_output", [
    ("north", np.array([-1, 0])),
    ("south", np.array([1, 0])),
    ("east", np.array([0, 1])),
    ("west", np.array([0, -1]))
])
def test_change_position(input_data, expected_output):
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    robot.change_position(input_data)
    assert np.all(robot.location == expected_output)


def test_load_save():
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    robot.location = np.array([3, 0])

    robot.warehouse.setCell(CornerCell(True, True, False, False), 2, 2)

    warehouse_name = "test"
    robot.save(warehouse_name)
    robot_reload: Robot = robot.load(f"maps/sandra/{warehouse_name}.pickle")

    assert np.all(robot.location == robot_reload.location)
    assert robot.warehouse.width == robot_reload.warehouse.width
    assert robot.warehouse.height == robot_reload.warehouse.height

    assert isinstance(robot_reload.warehouse.getCell(2, 2), CornerCell)


def test_robot_get_current_cell():
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()

    cell = robot.get_current_cell()

    assert isinstance(cell, CornerCell)

    robot.move("east")
    cell = robot.get_current_cell()

    assert isinstance(cell, HallwayCell)

    robot.reset_location()


@pytest.mark.parametrize("direction", ["north", "east", "west", "south"])
def test_robot_check_movement_possible(direction):
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/sandra/{warehouse_name}.pickle")
    robot.reset_location()
    cell = robot.get_current_cell()

    movement_possible = robot.check_movement_possible(direction, cell)
    if direction == "north":
        assert movement_possible is False
    elif direction == "south":
        assert movement_possible is True
    elif direction == "east":
        assert movement_possible is True
    elif direction == "west":
        assert movement_possible is False
