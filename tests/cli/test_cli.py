import pytest
import numpy as np

from cli import CLI
from commands import *
from commands.command_map import CommandMap
from robot.robot import Robot
from api.james_api import JamesResetResponse


@pytest.mark.parametrize("input_data, expected_output", [
    (["", "north"], "north"),
    (["", "south"], "south"),
    (["", "east"], "east"),
    (["", "west"], "west")
])
def test_cli_get_command(input_data, expected_output):
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    command = cli.get_command(input_data)
    assert isinstance(command, MoveCommand)
    assert command.direction == expected_output


def test_cli_invalid_command_raises():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    with pytest.raises(KeyError):
        command = cli.get_command(["", "bla"])


def test_cli_multiple_commands_raises():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    with pytest.raises(ValueError):
        command = cli.get_command(["", "north", "south"])


def test_cli_no_commands_raises():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    with pytest.raises(ValueError):
        direction = cli.get_command([""])


def test_cli_uppercase():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    command = cli.get_command(["", "NoRtH"])
    assert isinstance(command, MoveCommand)
    assert command.direction == "north"


def test_cli_capacity():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    command = cli.get_command([" ", "capacity"])
    assert isinstance(command, CalculateCapacityCommand)


def test_reset_command():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)

    robot.location = [1, 1]

    assert np.all(robot.location != np.array([0, 0]))

    command = cli.get_command([" ", "reset_state"])

    assert isinstance(command, ResetCommand)

    response = command.execute()

    assert np.all(robot.location == np.array([0, 0]))
    assert isinstance(response, JamesResetResponse)


def test_inventory_command():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)
    command = cli.get_command([" ", "inventory"])

    assert isinstance(command, InventoryCommand)


def test_shelf_content_command():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)
    command = cli.get_command([" ", "shelf_content"])

    assert isinstance(command, ShelfContentCommand)


def test_shelf_summary_command():
    session_id = "sandra"
    warehouse_name = "simple_warehouse"
    robot = Robot.load(f"maps/{session_id}/{warehouse_name}.pickle")
    command_map = CommandMap(robot)
    cli = CLI(command_map.command_map)
    command = cli.get_command([" ", "shelf_summary"])

    assert isinstance(command, ShelfSummaryCommand)
