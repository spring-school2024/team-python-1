from cells.cell import Cell


class RampCell(Cell):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        super().__init__(has_wall_north, has_wall_east, has_wall_south, has_wall_west)

        if sum([has_wall_south, has_wall_west, has_wall_east, has_wall_north]) != 3:
            raise AttributeError("Ramp cell must have three walls.")

    @property
    def name(self) -> str:
        return "Ramp"

    @property
    def capacity(self) -> int:
        return 0

    def __repr__(self):
        return f"Ramp, capacity: {self.capacity}"