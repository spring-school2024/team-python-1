from cells.cell import Cell


class XCrossingCell(Cell):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        super().__init__(has_wall_north, has_wall_east, has_wall_south, has_wall_west)

        if has_wall_north or has_wall_east or has_wall_west or has_wall_south:
            raise AttributeError("X-Crossing cell cannot have walls.")

    @property
    def name(self) -> str:
        return "X-Crossing"

    @property
    def capacity(self) -> int:
        return 4

    def __repr__(self):
        return f"X-crossing, capacity: {self.capacity}"
