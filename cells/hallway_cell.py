from cells.cell import Cell


class HallwayCell(Cell):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        super().__init__(has_wall_north, has_wall_east, has_wall_south, has_wall_west)

        if sum([has_wall_south, has_wall_west, has_wall_east, has_wall_north]) != 2:
            raise AttributeError("Hallway cell must have two walls.")
        if not (has_wall_south and has_wall_north) and not (has_wall_west and has_wall_east):
            raise AttributeError("Hallway cell must have two opposite walls.")

    @property
    def name(self) -> str:
        return "Hallway"

    @property
    def capacity(self) -> int:
        return 9

    def __repr__(self):
        return f"Hallway, capacity: {self.capacity}"