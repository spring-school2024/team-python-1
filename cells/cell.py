from abc import abstractmethod, ABC


class Cell(ABC):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        self.has_wall_north = has_wall_north
        self.has_wall_east = has_wall_east
        self.has_wall_south = has_wall_south
        self.has_wall_west = has_wall_west

    @property
    @abstractmethod
    def name(self) -> str:
        raise NotImplementedError

    @property
    @abstractmethod
    def capacity(self) -> int:
        raise NotImplementedError
