from cells.cell import Cell


class CornerCell(Cell):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        super().__init__(has_wall_north, has_wall_east, has_wall_south, has_wall_west)

        if sum([has_wall_south, has_wall_west, has_wall_east, has_wall_north]) != 2:
            raise AttributeError("Corner cell must have two walls.")
        if (has_wall_north and has_wall_south) or (has_wall_west and has_wall_east):
            raise AttributeError("Corner cell must have two adjacent walls.")

    @property
    def name(self) -> str:
        return "Corner"

    @property
    def capacity(self) -> int:
        return 8

    def __repr__(self):
        return f"Corner, capacity: {self.capacity}"
