from cells.cell import Cell


class EmptyCell(Cell):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        super().__init__(has_wall_north, has_wall_east, has_wall_south, has_wall_west)

        if sum([has_wall_south, has_wall_west, has_wall_east, has_wall_north]) != 4:
            raise AttributeError("Empty cell should have all walls to prevent the robot from entering it.")

    @property
    def name(self) -> str:
        return "Empty"

    @property
    def capacity(self) -> int:
        return 0

    def __repr__(self):
        return f"Empty, capacity: {self.capacity}"