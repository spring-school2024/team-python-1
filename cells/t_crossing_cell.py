from cells.cell import Cell


class TCrossingCell(Cell):
    def __init__(self, has_wall_north, has_wall_east, has_wall_south, has_wall_west):
        super().__init__(has_wall_north, has_wall_east, has_wall_south, has_wall_west)

        if sum([has_wall_south, has_wall_west, has_wall_east, has_wall_north]) != 1:
            raise AttributeError("T-Crossing cell must have one wall.")

    @property
    def name(self) -> str:
        return "T-Crossing"

    @property
    def capacity(self) -> int:
        return 6

    def __repr__(self):
        return f"T-crossing, capacity: {self.capacity}"