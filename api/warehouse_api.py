import requests

WAREHOUSE_API_URL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/"


class RobotStatusResponse:
    def __init__(self, response):
        self.request_status = response['request_status']
        self.id = response['id'] if self.request_status == 'ok' else None


class RobotMoveResponse:
    def __init__(self, response):
        self.request_status = response['request_status']
        self.message = response['message']


class RobotScanFarResponse:
    def __init__(self, response):
        self.request_status = response['request_status']
        if self.request_status == 'failed':
            self.message = response['message']
        else:
            self.message = None
        self.scan_info = response['scan_info'] if self.request_status == 'ok' else None


class RobotScanNearResponse:
    def __init__(self, response):
        self.request_status = response['request_status']
        if self.request_status == 'failed':
            self.message = response['message']
        else:
            self.message = None
        self.id = response['field_info']['id'] if self.request_status == 'ok' else None
        self.max_capacity = response['field_info']['max_capacity'] if self.request_status == 'ok' else None
        self.shelf_inventory = response['field_info']['shelf_inventory'] if self.request_status == 'ok' else None
        self.walls = response['field_info']['walls'] if self.request_status == 'ok' else None


class Warehouse_API:
    base_url = WAREHOUSE_API_URL

    def __init__(self, session: str = "sandra"):
        self.session = session

    # Robot commands
    def get_status(self):
        url = str(self.base_url + f'api/bot/{self.session}')
        response = requests.get(url)

        return RobotStatusResponse(response.json())

    def move(self, direction: str):
        url = str(self.base_url + f'api/bot/{self.session}/move/{direction}')
        response = requests.put(url)

        return RobotMoveResponse(response.json())

    def scan_far(self):
        url = str(self.base_url + f'api/bot/{self.session}/scan/far')
        response = requests.get(url)

        return RobotScanFarResponse(response.json())

    def scan_near(self):
        url = str(self.base_url + f'api/bot/{self.session}/scan/near')
        response = requests.get(url)

        return RobotScanNearResponse(response.json())

    # James commands
    def get_status(self):
        url = str(self.base_url + f'api/bot/{self.session}')
        response = requests.get(url)

        return RobotStatusResponse(response.json())
