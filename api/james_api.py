import requests

WAREHOUSE_API_URL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/"

class JamesResetResponse:
    def __init__(self, response):
        self.request_status = response['request_status']
        self.message = response['message']

class James_API:
    base_url = WAREHOUSE_API_URL

    def __init__(self, session: str = 'sandra'):
        self.session = session

    def reset(self):
        url = str(self.base_url + f'/api/james/{self.session}/reset')
        response = requests.put(url)

        return JamesResetResponse(response.json())
