import pickle
from typing import Union, Tuple, List, Dict

import numpy as np
from api.warehouse_api import Warehouse_API
from warehouse_system.warehouse import Warehouse
from cells import *


class Robot:
    def __init__(self, name, warehouse: Warehouse):
        self.name = name
        self.location = np.array([0, 0])
        self.__api = Warehouse_API(name)
        self.warehouse = warehouse

    def move(self, direction: str):
        '''Returns if the movement was successful or not (with a reason)'''

        current_cell = self.get_current_cell()
        movement_possible = self.check_movement_possible(direction, current_cell)
        if movement_possible:
            answer = self.__api.move(direction)
        else:
            return f'Movement not possible according to map (wall in the way).'

        if answer.request_status == 'ok':
            self.change_position(direction)
            self.save(self.warehouse.name)
            return f'Successfully moved to: {direction}'
        else:
            return f'Movement not possible: {answer.message}'

    def scan_far(self):
        '''Returns if the scan was successful and the distances to the walls in all four possible directions'''
        answer = self.__api.scan_far()

        if answer.request_status == 'ok':
            return f'Successfully scanned the area.'
        else:
            return f'Scan not possible: {answer.message}'

    def scan_near(self) -> Union[str, Tuple[str, Dict[str, bool], List[str]]]:
        '''Returns if the scan was successful and all the necessary field information'''
        answer = self.__api.scan_near()

        if answer.request_status == 'ok':
            return answer.id, answer.walls, answer.shelf_inventory
        else:
            return f'Scan not possible: {answer.message}'

    def change_position(self, direction: str):
        if direction == 'north':
            self.location[0] -= 1
        elif direction == 'south':
            self.location[0] += 1
        elif direction == 'east':
            self.location[1] += 1
        elif direction == 'west':
            self.location[1] -= 1
        else:
            return

    def reset_location(self):
        self.location = np.array([0, 0])

    def save(self, filename):
        with open(f'maps/{self.name}/{filename}.pickle', 'wb') as handle:
            pickle.dump(self, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load(filename):
        with open(filename, 'rb') as handle:
            robot = pickle.load(handle)

        return robot

    def get_current_cell(self) -> Cell:
        return self.warehouse.getCell(self.location[1], self.location[0])

    @staticmethod
    def check_movement_possible(direction, cell):
        if direction == "north":
            if cell.has_wall_north:
                return False
            else:
                return True
        elif direction == "east":
            if cell.has_wall_east:
                return False
            else:
                return True
        elif direction == "south":
            if cell.has_wall_south:
                return False
            else:
                return True
        elif direction == "west":
            if cell.has_wall_west:
                return False
            else:
                return True
