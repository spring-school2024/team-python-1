from typing import Tuple

from commands.command import Command
from commands.state_command import StateCommand
from robot.robot import Robot
from api.james_api import James_API


class ResetCommand(Command):
    def __init__(self, robot: Robot):
        self.robot = robot

    def execute(self):
        reset_response = James_API(self.robot.name).reset()
        if reset_response.request_status == "ok":
            self.robot.reset_location()
            self.robot.save(self.robot.warehouse.name)
        
        statecommand = StateCommand(self.robot)
        statecommand.execute()

        return reset_response

