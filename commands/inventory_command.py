from commands.command import Command
from robot.robot import Robot


class InventoryCommand(Command):
    def __init__(self, robot: Robot):
        self.robot = robot

    def execute(self):
        raise NotImplementedError()
    