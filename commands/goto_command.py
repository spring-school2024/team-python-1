import time

from commands.command import Command
from commands.move_command import MoveCommand
from robot.robot import Robot


class GoToCommand(Command):
    def __init__(self, robot: Robot, target_coordinate: str):
        self.robot = robot
        self.target_coordinate = target_coordinate

    def execute(self):
        path = self.robot.warehouse.get_shortest_seq(self.robot.get_current_cell(), self.robot.location, self.target_coordinate)
        for direction in path:
            time.sleep(1)
            move_command = MoveCommand(self.robot, direction)
            move_command.execute()
