from commands.command import Command
from warehouse_system.warehouse import Warehouse


class ShelfSummaryCommand(Command):
    def __init__(self, warehouse: Warehouse):
        self.warehouse = warehouse

    def execute(self):
        raise NotImplementedError
