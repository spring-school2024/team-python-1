from commands.command import Command
from robot.robot import Robot


class StateCommand(Command):
    def __init__(self, robot: Robot):
        self.warehouse = robot.warehouse
        self.robotLocation = robot.location

    def execute(self):
        print(self.warehouse.warehouseState(self.robotLocation))
    