from commands import *
from robot.robot import Robot
from typing import Dict

class CommandMap:
    def __init__(self, robot: Robot) -> None:
        self.robot = robot

    @property
    def command_map(self) -> Dict:
        command_map = {
            "north": MoveCommand(self.robot, "north"),
            "south": MoveCommand(self.robot, "south"),
            "west": MoveCommand(self.robot, "west"),
            "east": MoveCommand(self.robot, "east"),
            "state" : StateCommand(self.robot),
            "capacity": CalculateCapacityCommand(self.robot.warehouse),
            "reset_state": ResetCommand(self.robot),
            "inventory": InventoryCommand(self.robot),
            "shelf_content": ShelfContentCommand(self.robot.warehouse),
            "shelf_summary": ShelfSummaryCommand(self.robot.warehouse),
            "a0": GoToCommand(self.robot, "a0"),
            "a1": GoToCommand(self.robot, "a1"),
            "a2": GoToCommand(self.robot, "a2"),
            "b0": GoToCommand(self.robot, "b0"),
            "b1": GoToCommand(self.robot, "b1"),
            "b2": GoToCommand(self.robot, "b2"),
            "c1": GoToCommand(self.robot, "c1"),

        }
        return command_map
