from commands.command import Command
from commands.state_command import StateCommand
from robot.robot import Robot


class MoveCommand(Command):
    def __init__(self, robot: Robot, direction):
        self.robot = robot
        self.direction = direction

    def execute(self):
        response = self.robot.move(self.direction)
        statecommand = StateCommand(self.robot)
        statecommand.execute()
        return response
