# Warehouse CLI Python-Template

This is the template project for python development in the Festo Spring School. It should provide you a good starting point with some nice features built-in, which we'll explain in more detail as soon as the course starts. So far you'll get some hints here below for the most crucial commands and language specific topics.

## Setup

You can either use:
- The [GitPod](https://gitpod.io) integration which will spawn a VSCode in browser or you can link your local VSCode to the one Gitpod instance.
- Your own IDE and clone the repository to your local drive

We're making use of `pipenv` as virtual environment manager for Python. It basically stores all necessary information to setup a proper environment in the file `Pipfile`. `Pipfile.lock` contains the resolved dependencies (including versions and transient dependencies) so the resolution will go quicker the next time.

There are four basic commands to make use of pipenv:

- `pipenv install --dev`: Installs all dependencies and setup/update the virtual environment (by adding `--dev` also the developer dependencies)
- `pipenv shell` enters a new shell which contains all necessary packages installed (as done via `pipenv install`). So you use `python3 app.py` to start the application within this new shell as you would normally do.
- `pipenv install <package_name>` if you want to install a new package (by adding `--dev` you make sure to add it not for productive code but just for dev or testing purposes)
- `pipenv run <command>` starts the given `command` in the virtual environment setup via pipenv. E.g `pipenv run python3 app.py` to start the application.

If you happen to use Linux (or Gitpod which is based on Linux) you can also make use of the integrated Makefile which knows the following targets (type them into the console):

- `make setup`: Setup the pipenv environment
- `make test`: Run all tests
- `make coverage`: Run all tests and record the code coverage for these tests
- `make all`: Setup the environment (if necessary) and run all tests and coverage

## Getting Started

We provided you with some basic command line application as a starting point and some tests to show how it could be done. Feel free to implement your own structure. Nevertheless we suggest to keep the existing main folder structure:

- `warehouse_system`: This is the module where you can start implementing your own logic and create your own structure underneath
- `tests`: Here you'll put the tests

## Testing

- We integrated [pytest](https://docs.pytest.org) as testing framework and some examples in the tests folder
- Better readable assertions with [PyHamcrest](https://github.com/hamcrest/PyHamcrest)